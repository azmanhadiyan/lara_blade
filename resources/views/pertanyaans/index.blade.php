@extends('../master')

@section('content')
<a href="{{ route('pertanyaan.create')}}" class="btn btn-primary">Tambah</a><br><br>
 
  <table class="table table-striped border text-center">
    <thead>
        <tr>
          <td>No</td>
          <td>Judul</td>
          <td>Isi</td>
          <td colspan="2">Kelola Data</td>
        </tr>
    </thead>
    <tbody>
        @foreach($pertanyaan as $pertanyaan)
      <tr>
          <td>{{$pertanyaan->id}}</td>
          <td>{{$pertanyaan->judul}}</td>
          <td>{{$pertanyaan->isi}}</td>
          <td>
              <a href="{{ route('pertanyaan.edit', $pertanyaan->id)}}" class="btn btn-warning">Edit</a>
              
              {{ csrf_field() }}
              <input type="hidden" name="_method" value="Delete">
              <form action="{{ route('pertanyaan.destroy', $pertanyaan->id)}}" method="post">
              <button class="btn btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
         </td>
      </tr>
      @endforeach
    </tbody>
  </table>
@endsection