<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
		$pertanyaan = "";
   		return view('pertanyaans.create', compact('pertanyaan'));
   		// return View::make('pertanyaan.storeOrUpdate');
	}

	public function index()
	{
	    $pertanyaans = Pertanyaan::all();
	    return view('pertanyaans.index', compact('pertanyaans'));
	    dd($pertanyaans);
    }
    
    public function store(Request $request){
	    // $validasi = $this->validate($request, [
	    //     'judul' => 'required',
	    //     'isi'=> 'required'
	    // ]);

	    // $pertanyaan = new Pertanyaan;
	    // $pertanyaan->judul = $request->judul;
	    // $pertanyaan->isi = $request->isi;
        // $pertanyaan->save();
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

	    return redirect('/pertanyaans')->with('success', 'Selamat data berhasil ditambah!');
	}

	public function edit($id)
	{
		$pertanyaan = Pertanyaan::find($id);

   		return view('pertanyaans.edit', compact('pertanyaan'));
	}

    public function update(Request $request, $id)
	{
	    $validasi = $this->validate($request, [
	        'judul' => 'required',
	        'isi'=> 'required'
	    ]);

	    $pertanyaan = Pertanyaan::find($id);
	    $pertanyaan->judul = $request->judul;
	    $pertanyaan->isi = $request->isi;
	    $pertanyaan->save();

	    return redirect('/pertanyaans')->with('success', 'Data berhasil di update');
	}

	public function destroy($id)
	{
	    $pertanyaans = Pertanyaan::findOrFail($id);
	    $pertanyaans->delete();

	    return redirect('/pertanyaans')->with('success', 'Data berhasil dihapus!');
	}
}
